# Pathfinder Bestiary Database
A SQLite Pathfinder Bestiary Database containing 3082 records.

## Why SQLite?
SQLite is portable and easily migratable to other databases. Ultimately, SQLite is unlikely to meet the needs of the project I am working on but for now it is good for getting my project up and running quickly and easier to share than anything else.

## Bad Schema
Yeah, I know this is the worst schema you've ever seen. How many columns in a single table?! Don't worry about it. Consider this an alpha version of the database. I will publish future versions with a better architected schema.

UPDATE: This still isn't really where I want it but I've started pulling this apart into meaningful relational tables.

## Where did I get all this data from?
[here](http://www.pathfindercommunity.net/home/databases/full-bestiary)

## How can I help?
I migrated this DB from an ODS document and did not spend any time checking to make sure all of the columns made sense or doing any actual schema architecture. As a result this is a single table with 96 columns. Yeah, that's bad and I should be ashamed. I will fix this but if you are reading this and feel like you have the time or are just annoyed enough then please let me know what you plan to do and help split up this database for me. If no one else gets to it then I eventually will.
